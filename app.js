var app = require('express')();
var Raven = require('raven');

// Must configure Raven before doing anything else with it
Raven.config('https://5e66865d66a34202bf674a41346ab1b6:a7436f8077dc4283863e20b9f5c2a8d1@sentry.io/285515').install();

// The request handler must be the first middleware on the app
app.use(Raven.requestHandler());

app.get('/', function mainHandler(req, res) {
    throw new Error('Broke!');
});

// The error handler must be before any other error middleware
app.use(Raven.errorHandler());

// Optional fallthrough error handler
app.use(function onError(err, req, res, next) {
    // The error id is attached to `res.sentry` to be returned
    // and optionally displayed to the user for support.
    res.statusCode = 500;
    res.end(res.sentry + '\n');
});

app.listen(3000);